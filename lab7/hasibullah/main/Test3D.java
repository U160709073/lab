package hasibullah.main;
import hasibullah.shapes3d.*;

public class Test3D {

	public static void main(String[] args) {
		
		Cylinder cylinder=new Cylinder();

		System.out.println(cylinder.area());
		System.out.println(cylinder.volume());
		System.out.println(cylinder.toString());
		System.out.println();
		
		Cube cube = new Cube();

		System.out.println(cube.area());
		System.out.println(cube.volume());
		System.out.println(cube.toString());
	}

}
