package hasibullah.shapes3d;

public class Square {
	protected int rib;
	public Square(int rib){
		this.rib = rib;
	}
	public Square(){
		this.rib = 5;
	}
	public int area(){
		return rib * rib;
	}
}
