package hasibullah.shapes3d;
import hasibullah.shapes.Circle;

public class Cylinder extends Circle{

	int height;

	public Cylinder(int h, int r){
		height = h;
		radius = r;
	}
	public Cylinder(){
		height = 4;
		radius = 5;
	}
	public double area() { 										// A=2πrh+2πr^2
		return (2 * super.area()) + (2 * Math.PI * radius * height);
	}

	public double volume() {								 // V=πr^2h
		return super.area() * height;
	}

	public String toString() {
		return "radius = " + radius + ", " + " height = " + height;
	}

}
