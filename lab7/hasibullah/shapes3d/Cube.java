package hasibullah.shapes3d;

public class Cube extends Square {
	
	public Cube(int side){
		super.rib = side;
	}
	public Cube(){
		super.rib = 5;
	}
	
	public int area(){											// A=6a^2
		return 6 * super.area();
	}
	
	public int volume(){										// V=a^3
		return rib * super.area();
	}
	
	public String toString(){
		return "Rib = " + rib + "," + " Area = " + area() + "," + " Volume = " + volume();
	}

}
