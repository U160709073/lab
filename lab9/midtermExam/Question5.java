package midtermExam;

public class Question5 {
	
	public static void main(String[] args) {
		int[][] matrix = { { 2, 5 }, { 4, 5 } };

		System.out.println(CalculateAverage(matrix));
	}

	public static int CalculateAverage(int[][] A) {
		int count = 0;
		int sum = 0;
		for (int i = 0; i < A.length; i++) {
			for (int j = 0; j < A[i].length; j++) {
				sum = sum + A[i][j];
				count++;
			}
		}
		return sum / count;
	}
}