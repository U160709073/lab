package midtermExam;

public class Question2 {

	public static void main(String[] args) {
		System.out.println(min(5, 5, 5));
	}

	private static int min(int a, int b, int c) {
		int min = a < b ? a : b;
		return min < c ? min : c;
	}
}					// 5
