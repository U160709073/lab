package midtermExam;

public class Question7 {

	public static void main(String[] args) {
		System.out.println(powN(3, 3));
	}

	private static int powN(int num, int pow) {
		if (pow == 0) {
			return 1;
		}
		return num * powN(num, pow - 1);
	}
}
