package drawingV3;

import java.util.ArrayList;

public class Drawing {
	private ArrayList<Shape> shapes = new ArrayList<Shape>();

	public void addShape(Shape obj) {
		shapes.add(obj);
	}

	public double calculateTotalArea() {
		double totalArea = 0;
		for (Shape obj : shapes) {
			totalArea += obj.area();

		}

		return totalArea;
	}

}
