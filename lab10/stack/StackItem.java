package stack;

public class StackItem<T> {
	private T value;
	private StackItem<T> next;

	public StackItem(T value, StackItem<T> next) {
		this.value = value;
		this.next = next;
	}

	public T getValue() {
		return value;
	}

	public StackItem<T> getNext() {
		return next;
	}
}
