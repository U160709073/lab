package stack;

import java.util.ArrayList;
import java.util.List;

public class StackArrayListImpl<T> implements Stack<T> {

	private ArrayList<T> stacklist = new ArrayList<T>();

	public void push(T t) {
		stacklist.add(0,t);
	}

	public T pop() {

		return stacklist.remove(0);
		
	}

	public boolean empty() {

		return stacklist.size() == 0;
	}

	public List<T> toList() {
		return stacklist;
	}

}