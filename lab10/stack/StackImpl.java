package stack;

import java.util.ArrayList;
import java.util.List;

public class StackImpl<T> implements Stack<T> {
	private StackItem<T> head;

	public void push(T value) {
		StackItem<T> newStack = new StackItem<T>(value, head);
		head = newStack;
	}

	public T pop() {
		T value = head.getValue();
		head = head.getNext();
		return value;
	}

	public boolean empty() {
		return head == null;
	}

	public List<T> toList() {
		StackItem<T> current = head;
		ArrayList<T> arraylist = new ArrayList<T>();
		while (current != null) {
			arraylist.add(0, current.getValue());
			current = current.getNext();
		}
		return arraylist;
	}
}
