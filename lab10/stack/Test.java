package stack;

public class Test {

	public static <T> void main(String[] args) {
		StackArrayListImpl<String> arrayStack = new StackArrayListImpl<String>();
		arrayStack.push("A");
		arrayStack.push("B");
		System.out.println(arrayStack.pop());
		
		StackImpl<String> stack = new StackImpl<String>();
		stack.push("World");
		stack.push("ABC");
		System.out.println(stack.toList());
	}

}